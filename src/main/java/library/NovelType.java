package library;

public enum NovelType {
    Fantasy, Horror, Mystery, Romance, Western, Crime, Humor, Satire
}
