package library;

public class Novel extends Book {
    private NovelType type;

    public Novel(String name, int numberOfPages, NovelType type) {
        super(name, numberOfPages);
        this.type = type;
    }

    public NovelType getType() {
        return type;
    }

    @Override
    public String toString() {
        return "Novel{" +
                "type=" + type +
                ", name='" + name + '\'' +
                ", numberOfPages=" + numberOfPages +
                '}';
    }
}
