package library;

import library.*;

public class Main {

    public static void main(String[] args) {
        Book b1 = new ArtAlbum("My album", 56, 4);
        Book b2 = new ArtAlbum("My second album", 20, 5);
        Book b3 = new ArtAlbum("My third album", 53, 6);
        Book b4 = new ArtAlbum("My fourth album", 50, 10);

        Book b5 = new Novel("The Return", 325, NovelType.Romance);
        Book b6 = new Novel("The Handmaid's Tale", 289, NovelType.Fantasy);
        Book b7 = new Novel ("Drakula", 250, NovelType.Horror);

        Library library = new Library();
        library.addBook(b2);
        library.addBook(b3);
        library.addBook(b4);
        library.addBook(b5);
        library.addBook(b6);
        library.addBook(b7);

        library.listBooks();

        library.deleteBook(b3);

        library.listBooks();
    }

}
