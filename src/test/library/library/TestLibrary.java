package library.library;


import library.Book;
import library.Library;
import org.junit.Assert;
import org.junit.Test;

import java.util.Random;

import static org.mockito.Mockito.mock;




public class TestLibrary {

    @Test
    public void testAddBook(){
        Library library = new Library();
        Book book = mock(Book.class);
        library.addBook(book);
        Assert.assertTrue(library.getBooks().contains(book));
    }
    @Test
    public void testDeleteBook(){
        Library library = new Library();
        Book book = mock(Book.class);
        library.addBook(book);
        library.deleteBook(book);
        Assert.assertFalse(library.getBooks().contains(book));
    }

    @Test
    public void testGetBooks(){

        Random r = new Random();
        int low = 2;
        int high = 50;
        int result = r.nextInt(high-low) + low;
        Library library = new Library();

        for (int i=0; i<result; i++){
            Book book = mock(Book.class);
            library.addBook(book);
        }

        Assert.assertEquals(result,library.getBooks().size());
    }

}
